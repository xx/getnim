# This file is embedded in the `getnim` executable and is written to
# ~/.nimble/bin/. It emulates a portable symlink with some nice additional
# features.

import strutils, os, osproc

import nimblepkg/[cli, options, version]
import nimblepkg/common as nimbleCommon
import cliparams
from common import GetnimError, mingwProxies

proc getSelectedPath(params: CliParams): string =
  var path = ""
  try:
    path = params.getCurrentFile()
    if not fileExists(path):
      let msg = "No installation has been chosen. (File missing: $1)" % path
      raise newException(GetnimError, msg)

    result = readFile(path)
  except Exception as exc:
    let msg = "Unable to read $1. (Error was: $2)" % [path, exc.msg]
    raise newException(GetnimError, msg)

proc getExePath(params: CliParams): string
  {.raises: [GetnimError, ValueError].} =
  try:
    let exe = getAppFilename().extractFilename
    let exeName = exe.splitFile.name

    if exeName in mingwProxies and defined(windows):
      return getMingwBin(params) / exe
    else:
      return getSelectedPath(params) / "bin" / exe
  except Exception as exc:
    let msg = "getAppFilename failed. (Error was: $1)" % exc.msg
    raise newException(GetnimError, msg)

proc main(params: CliParams) {.raises: [GetnimError, ValueError].} =
  let exePath = getExePath(params)
  if not fileExists(exePath):
    raise newException(GetnimError,
        "Requested executable is missing. (Path: $1)" % exePath)

  try:
    # Launch the desired process.
    let p = startProcess(exePath, args=commandLineParams(),
                         options={poParentStreams})
    let exitCode = p.waitForExit()
    p.close()
    quit(exitCode)
  except Exception as exc:
    raise newException(GetnimError,
        "Spawning of process failed. (Error was: $1)" % exc.msg)

when isMainModule:
  var error = ""
  var hint = ""
  var params = newCliParams(proxyExeMode = true)
  try:
    parseCliParams(params, proxyExeMode = true)
    main(params)
  except NimbleError as exc:
    (error, hint) = getOutputInfo(exc)

  if error.len > 0:
    displayTip()
    display("Error:", error, Error, HighPriority)
    if hint.len > 0:
      display("Hint:", hint, Warning, HighPriority)

#    display("Info:", "If unexpected, please report this error to " &
#            "https://github.com/dom96/choosenim", Warning, HighPriority)
    quit(1)
