@echo off
.\getnim\getnim.exe stable --firstInstall

for /f "delims=" %%a in ('.\getnim\getnim.exe --getNimbleBin') do @set NIMBLEBIN=%%a
copy .\getnim\getnim.exe "%NIMBLEBIN%\getnim.exe"

echo             Work finished.
echo             Now you must ensure that the Nimble bin dir is in your PATH:
echo               %NIMBLEBIN%

pause
